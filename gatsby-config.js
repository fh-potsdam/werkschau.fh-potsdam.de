module.exports = {
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `assets`,
				path: `${ __dirname }/src/content`,
			},
		},
		{
			resolve: `gatsby-transformer-remark`,
			options: {
				plugins: [
					{
						resolve: `gatsby-remark-images`,
						options: {
							maxWidth: 1600
						}
					}
				]
			}
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `Werkschau`,
				short_name: `Werkschau`,
				start_url: `/`,
				background_color: `#000000`,
				theme_color: `#FFFFFF`,
				icon: "static/images/favicon/favicon.png",
				display: `standalone`
			},
		},
		`gatsby-plugin-offline`,
		`gatsby-plugin-sass`
	]
};
