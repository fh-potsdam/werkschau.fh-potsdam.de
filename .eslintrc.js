module.exports = {
    "extends": "react-app",
    "rules": {
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double",
	        { "allowTemplateLiterals": true }
        ],
        "semi": [
            "error",
            "always"
        ],
	    "no-console": 0,
	    "indent": [
	    	"error",
		    "tab",
		    { "SwitchCase": 1 }
	    ],
	    "react/jsx-uses-vars": 1,
	    "import/newline-after-import": [
		    "error",
		    { "count": 2 }
	    ],
	    "react/jsx-curly-spacing": [
	    	2,
		    "always",
		    {
		        "allowMultiline": true,
		        "spacing": {
		        	"objectLiterals": "always"
		        }
	        }
	    ],
	    "keyword-spacing": [2]
    }
};