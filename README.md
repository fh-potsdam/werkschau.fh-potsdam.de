
# 🎉 Werkschau Website Onboarding

Example Website → https://werkschau.coderwelsch.com

![Werkschau Website](./readme-content/teaser-image.png)

Dear future Werkschau website advisor, thank you very much for taking care of our website project and helping us students making the Werkschau great!

Please read the following onboarding guide carefully to setup this code base and git repo for the next Werkschau.

## Where is the 💎 Sketch file?

You can download the sketch file here: [https://sketch.cloud/s/OnDjw](https://sketch.cloud/s/OnDjw). 

## Required Knowledge / Toolset

Please note that you should know the used tools and concepts in this code base:

 - NPM / YARN
 - [Reactjs](https://reactjs.org/)
 - [Gatsbyjs](https://www.gatsbyjs.org/)
 - [GraphQL](https://graphql.org/learn/)
 - [SCSS (SASS)](https://sass-lang.com/)
 - [BULMA](https://bulma.io/) 
 - [Markdown](https://markdown.de/)
 - [Terminal](https://www.maclife.de/tipps-tricks/software/os-x-das-terminal-fuer-einsteiger)
 - [GIT]()https://rogerdudler.github.io/git-guide/index.de.html

### What is Gatsbyjs???

This website was built on top of [gatsbyjs](https://www.gatsbyjs.org), a static site generator toolset for building fast, reliable, seo friendly and “prerendered” react based progressive web apps. If you are not familiar with gatsbyjs you should give you a “small” [introducing tutorial](https://www.youtube.com/watch?v=6YhqQ2ZW1sc). 

Gatsbyjs helps me a lot by processing ”non code“. By that I mean images, videos, text data and so on. Images, vector graphics and videos are cropped and resized for various browser sizes and pixel densities (aka [`<img srcset="…" />`](https://blog.kulturbanause.de/2014/09/responsive-images-srcset-sizes-adaptive/)). You really should give it a try.

## Setup

### Download & Installation

To install and download the code base please send me (Joseph Ribbe, [social@coderwelsch.com](mailto:social@coderwelsch.com?subject=Werkschau%20Repo%20Authorisierung)) a request, so that I can authorize you to the git repo. You have to signup on [gitlab.com](https://gitlab.com/) for an account.

If you’re authorized, execute the following command in your terminal:

#### Download

```shell script
git clone https://gitlab.com/Coderwelsch/werkschau.fh-potsdam.de.git
```

#### Install

After cloning the repo just ”cd“ into the repo folder and hit:

```shell script
npm i # or `yarn`
```

#### Start Watch Tasks

To start the development watch task type:

```shell script
npm start # or `yarn start`
```

You can now open the url [http://localhost:8000/](http://localhost:8000/) in your browser.

## Build and Upload

### Build

If you’re ready to build just type:

```shell script
npm build:production # or `yarn build:production`
```

This command will clear at first all cached files and generates a bundle of static html, javascript and asset files under `./public`. You could upload that folder via SFTP (please do not use FTP!). There are no automatic continuous integration or deployment scripts set up, so you have to synchronize the files by your own (I use [Transmit](https://panic.com/transmit/) for that, it’s really cool and it’s worth every penny).

### Upload

**SFTP Login Data:**

Server: `werkschau.fh-potsdam.de`  
Username: `werkschau`  
Password: `please ask me 😇 …`  
Path on Server: `/public`


## Folder Structure

![Folder Structure](./readme-content/file-structure.png)

The `./src/` folder contains:

 - `./src/assets/`  – fonts, general svg icons
 - `./src/components/` – re-usable react components
 - `./src/content/` – images and texts, written in Markdown
 - `./src/pages/` – entry pages like the index.jsx (renders to .html) page, written as react components
 
 
## How things works
 
To dive into this code base, you should enter at first the `./src/pages/index.jsx` file. This file renders the main landing page for https://werkschau.fh-potsdam.de/. You can find other .jsx files in the `./src/pages/` folder like:
 
 - `imprint.jsx` → renders to website route `/imprint` 
 - `data-privacy.jsx` → renders to website route `/data-privacy` 
 
### How I will receive my data like images, texts and videos?
 
Under `./src/content/` you can find numerous markdown files (.md). Markdown is a very easy way to write formatted texts and store data like lists or arrays, numbers and referencing other markdown files.  
 
When you jump into the `./src/pages/index.jsx` source code you will find a GraphQL query at the end of the file:
```javascript
export const query = graphql`
    query {
        general: markdownRemark (frontmatter: { section: { eq: "general" } }) {
            frontmatter {
                seo {
                    title
                    description
                }
    
                navItems {
                    title
                    link
                }
    
                footerItems {
                    title
                    items {
                        title
                        link
                    }
                }
            }
        }
    }
`;
```

Markdown data (in this case: `src/content/00_general.md`) will be passed from gatsby to every component via a graphql query.

### Markdown Data

When you take a look into the `src/content/00_general.md` file you will find a “header” declaration and the typical markdown text content.

The header looks like:

```markdown
---
section: "general"

seo:
 title: "Werkschau ’19" 
 description: "12.–13. Juli 2019 • Die Werkschau 2019 der FH Potsdam ist die studentisch geplante und realisierte Jahresausstellung des Fachbereichs Design. Hier haben Studierende aus den vier Studiengängen Interfacedesign, Kommunikationsdesign, Produktdesign und dem Master in Design die Möglichkeit, ihre Arbeiten und Projekte aus den vergangenen zwei Semestern auszustellen und zu präsentieren."
----
``` 

The markdown header data are accessible via `data.general.frontmatter.seo`. Example:

```jsx harmony
export default ({ data }) => 
    <BreakpointProvider>
        <Page
            seo={ data.general.frontmatter.seo }
            nav={ data.general.frontmatter.navItems }>
            /* ... */
        </Page>
    </BreakpointProvider>
``` 



## Open ToDos (for you 😜)
- [ ] Setup of Continuous Deployment Scripts for Gitlab
- [ ] More different animated flowers!
- [ ] Implementation of the interactive exhibition map for Werkschau ’20