import React, { Component } from "react";
import PropTypes from "prop-types";

import SectionComp from "react-bulma-components/lib/components/section/section";

import Styles from "./index.module.scss";


export default class Section extends Component {
	static propTypes = {
		isDark: PropTypes.bool,
		hasBottomSeparator: PropTypes.bool
	};

	static defaultProps = {
		hasBottomSeparator: true
	};
	
	render () {
		const
			classNames = [ Styles.section ],
			{
				className,
				isDark,
				hasBottomSeparator,
				...props
			} = this.props;

		if (className) {
			classNames.push(className);
		}

		if (isDark) {
			classNames.push("has-background-dark");
		}

		return (
			<React.Fragment>
				{ isDark &&
					Section.renderSeparator()
				}

				<SectionComp
					className={ classNames.join(" ") }
					{ ...props }
				/>

				{ isDark && hasBottomSeparator &&
					Section.renderSeparator(true)
				}
			</React.Fragment>
		);
	}

	static renderSeparator (isBottom) {
		const classNames = [
			Styles.isDark
		];

		if (isBottom) {
			classNames.push(Styles.isBottom);
		}

		return (
			<div className={ classNames.join(" ") } />
		);
	}
}
