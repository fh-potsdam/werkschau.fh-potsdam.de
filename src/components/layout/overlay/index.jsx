import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";

import Styles from "./index.module.scss";


export default class Overlay extends Component {
	static propTypes = {
		active: PropTypes.bool,
		onClose: PropTypes.func,
		className: PropTypes.string,
		contentClassName: PropTypes.string,
		closeIconClassName: PropTypes.string
	};

	constructor (props) {
		super(props);

		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.handleScrolling = this.handleScrolling.bind(this);
	}

	componentDidMount () {
		document.addEventListener("keydown", this.handleKeyPress, false);
		window.addEventListener("touchmove", this.handleScrolling, false);
	}

	componentWillUnmount () {
		document.removeEventListener("keydown", this.handleKeyPress);
	}

	componentDidUpdate (prevProps, prevState, snapshot) {
		if (this.props.active) {
			document.documentElement.style.overflow = "hidden";
		} else {
			document.documentElement.style.overflow = "";
		}
	}

	handleScrolling (event) {
		if (this.props.active) {
			event.preventDefault();
		}
	}

	handleKeyPress (event) {
		if (!this.props.active) {
			return false;
		}

		if (event.keyCode === 27 && typeof this.props.onClose === "function") {
			this.props.onClose();

		}

		event.preventDefault();
		event.stopImmediatePropagation();

		return false;
	}

	render () {
		// server side rendering problem fix
		if (!this.props.active || typeof window === "undefined") {
			return null;
		}

		const {
			contentClassName,
			closeIconClassName
		} = this.props;

		return ReactDOM.createPortal(
			<div className={ this.generateClass() } data-active={ this.props.active || undefined }>
				<div className={ `${ Styles.content } ${ contentClassName ? contentClassName : "" }` }>
					<div
						onClick={ this.props.onClose }
						className={ Styles.closeButton + (closeIconClassName ? " " + closeIconClassName : "") }>
						×
					</div>

					<div className="children">
						{ this.props.children }
					</div>
				</div>

				<div className={ Styles.background } />
			</div>,
			window.document.body
		);
	}

	generateClass () {
		const classes = [ Styles.overlay ];

		if (this.props.active) {
			classes.push(Styles.active);
		}

		if (this.props.className) {
			classes.push(this.props.className);
		}

		return classes.join(" ");
	}
}
