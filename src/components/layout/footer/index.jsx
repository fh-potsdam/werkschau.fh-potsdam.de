import React, { Component, PureComponent } from "react";
import PropTypes from "prop-types";
import { graphql, Link, StaticQuery } from "gatsby";

import FooterComp from "react-bulma-components/lib/components/footer/footer";
import Container from "react-bulma-components/lib/components/container/container";
import Columns from "react-bulma-components/lib/components/columns/columns";
import Heading from "react-bulma-components/lib/components/heading/heading";


export default class Footer extends PureComponent {
	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
						general: markdownRemark (frontmatter: { section: { eq: "general" } }) {
							frontmatter {
								seo {
									title
									description
								}
								
								navItems {
									title
									link
								}
								
								footerItems {
									title
									items {
										title
										link
									}
								}
							}
						}
					}` }
				render={ this.renderContent.bind(this) }
			/>
		);
	}

	renderContent ({ general }) {
		const { footerItems } = general.frontmatter;

		return (
			<React.Fragment>
				<FooterComp className={ "has-background-grey-darker has-padding-top-xxl" }>
					<Container>
						<Columns>
							{ footerItems.map(({ title, items }) =>
								<Columns.Column
									key={ title }
									size={ "one-third" }>

									<Heading
										size={ 2 }
										renderAs={ "h1" }
										className={ "has-text-white" }>
										{ title }
									</Heading>

									<ul>
										{ items.map(({ title, link }) =>
											<li key={ title }>
												<a
													href={ link }
													className={ "has-text-white" }
													target={ "_blank" }>
													{ title }
												</a>
											</li>
										) }
									</ul>
								</Columns.Column>
							) }
						</Columns>
					</Container>
				</FooterComp>

				<FooterComp
					className={ "has-background-dark is-block has-padding-md" }
					renderAs={ "a" }
					href={ "https://www.coderwelsch.com" }
					target={ "_blank" }>

					<Container>
						<Columns>
							<Columns.Column>
								<Heading
									className={ "has-text-white has-text-centered is-italic has-text-link" }
									subtitle>
									Made with ❤️ by Joseph Ribbe
								</Heading>
							</Columns.Column>
						</Columns>
					</Container>
				</FooterComp>
			</React.Fragment>
		);
	}
}
