import React, { Component } from "react";
import PropTypes from "prop-types";

import Nav from "react-bulma-components/lib/components/navbar/navbar";
import Container from "react-bulma-components/lib/components/container/container";
import ScrollTo from "animated-scroll-to";

import Styles from "./index.module.scss";

import LogoWithTextSrc from "../../../../assets/svg/semicolon-white.svg";


export default class Navigation extends Component {
	static propTypes = {
		items: PropTypes.array.isRequired
	};

	handleLinkClick (id) {
		const element = document.getElementById(id);

		if (element) {
			ScrollTo(
				document.querySelector(`#${ id }`),
				{
					offset: -80,
					speed: 750
				}
			);
		}
	}
	
	render () {
		return (
			<Nav
				color={ "dark" }
				fixed={ "top" }
				className={ Styles.navComponent }>
				<Container className={ Styles.navContainer }>
					<Nav.Brand
						className={ Styles.logoContainer }
						renderAs={ "a" }
						href={ "/" }>

						<img
							src={ LogoWithTextSrc }
							className={ Styles.logo }
						    alt=""
						/>
					</Nav.Brand>

					<Nav.Container position={ "end" } className={ Styles.navEndContainer }>
						{ this.props.items.map(({ title, link }) =>
							<Nav.Item
								key={ title }
								renderAs={ "div" }
								className={ Styles.navItemContainer }>
								<a
									onClick={ this.handleLinkClick.bind(this, link) }
									className={ `has-text-weight-bold is-size-5 has-text-white ${ Styles.navItem }` }>
									{ title }
								</a>
							</Nav.Item>
						) }
					</Nav.Container>
				</Container>
			</Nav>
		);
	}
}
