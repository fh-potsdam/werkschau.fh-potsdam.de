import React, { Component } from "react";
import PropTypes from "prop-types";

import SEO from "../../seo";
import Navigation from "./navigation";


export default class Page extends Component {
	static propTypes = {
		children: PropTypes.any.isRequired
	};
	
	render () {
		return (
			<React.Fragment>
				<SEO { ...this.props.seo } />

				<Navigation items={ this.props.nav } />

				{ this.props.children }
			</React.Fragment>
		);
	}
}
