import React from "react";


const InDev = props => process.env.NODE_ENV === "development" ? props.children : null;
const InProd = props => process.env.NODE_ENV === "production" ? props.children : null;

export default InDev;

export {
	InDev,
	InProd
};
