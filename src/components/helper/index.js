const shuffleArray = array  => {
	const clonedArray = JSON.parse(JSON.stringify(array));
	let counter = clonedArray.length;

	// While there are elements in the array
	while (counter > 0) {
		// Pick a random index
		let index = Math.floor(Math.random() * counter);

		// Decrease counter by 1
		counter--;

		// And swap the last element with it
		let temp = clonedArray[counter];
		clonedArray[counter] = clonedArray[index];
		clonedArray[index] = temp;
	}

	return clonedArray;
};

function cssPropertyValueSupported (prop, value) {
	const d = document.createElement("div");
	d.style[prop] = value;
	return d.style[prop] === value;
}

function applyHangingPunctuation (string = "") {
	if (typeof CSS !== "undefined" && CSS.supports("hanging-punctuation")) {
		return string;
	}

	const punctuations = ["„", "»", "“"];

	for (const punctuation of punctuations) {
		// first letter
		if (string.startsWith(punctuation)) {
			string = `<span style="margin-left: -1ex;">${ punctuation }</span>` + string.substring(1);
			break;
		}
	}

	for (const punctuation of punctuations) {
		const replacement = `<span style="margin-left: -1ex;">${ punctuation }</span>`;

		// other
		string = string.replace(new RegExp(`<br>(\n)?${ punctuation }`, "gi"), "<br>" + replacement);
		string = string.replace(new RegExp(`<p>${ punctuation }`, "gi"), "<p>" + replacement)
	}


	return string;
}

export {
	applyHangingPunctuation,
	shuffleArray
};