import React, { Component } from "react";
import Slider from "react-slick";
import { graphql, StaticQuery } from "gatsby";
import Image from "gatsby-image";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Button from "react-bulma-components/lib/components/button/button";

import Flower from "../../ui/decoration/flower-1";
import Overlay from "../../layout/overlay";
import { shuffleArray } from "../../helper";
import Styles from "./index.module.scss";


export default class Hero extends Component {
	static SETTINGS = {
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 500,
		dots: false,
		arrows: false,
		infinite: true,
		lazyLoad: "ondemand",
		pauseOnHover: false
	};

	shuffledTrailer = [];

	state = {
		overlayActive: false
	};

	openOverlay () {
		const firstItem = this.shuffledTrailer.pop();

		this.shuffledTrailer = [ firstItem, ...this.shuffledTrailer ];

		this.setState({
			...this.state,
			overlayActive: true
		});
	}

	closeOverlay () {
		this.setState({
			...this.state,
			overlayActive: false
		});
	}

	renderContent (data) {
		const { slides, overlay, trailer } = data.hero.frontmatter;

		if (!this.shuffledTrailer.length) {
			this.shuffledTrailer = shuffleArray(trailer);
		}

		return (
			<div className={ Styles.hero }>
				<Overlay
					className={ Styles.overlay }
					contentClassName={ Styles.overlayContent }
					closeIconClassName={ Styles.overlayCloseIcon }
					active={ this.state.overlayActive }
					onClose={ this.closeOverlay.bind(this) }>
					{ this.state.overlayActive && this.renderVideo() }
				</Overlay>

				<div
					className={ Styles.videoBtnWrapper }
					onClick={ this.openOverlay.bind(this) }>
					<div className={ Styles.circle } />

					<Button className={ Styles.videoBtn }>
						Trailer ansehen
					</Button>
				</div>

				<div className={ Styles.semicolonWrapper }>
					<div className={ Styles.semicolon }>
						<Heading
							renderAs={ "h1" }
							className={ Styles.headline }
							dangerouslySetInnerHTML={ { __html: overlay.title } }
						/>

						<Heading
							renderAs={ "h2" }
							className={ Styles.subline }
							dangerouslySetInnerHTML={ { __html: overlay.subtitle } }
						/>
					</div>
				</div>

				<Slider { ...Hero.SETTINGS }>
					{ this.renderSlides(slides) }
				</Slider>

				<div className={ Styles.flowersContainer }>
					<Flower />
					<Flower />
					<Flower />
					<Flower />
					<Flower />
					<Flower />
				</div>
			</div>
		);
	}

	renderSlides (slides) {
		return slides.map(({ title, image }) =>
			<div
				key={ title }
				className={ Styles.slideWrapper }>

				<div className={ Styles.slide }>
					<Image
						className={ Styles.slideImg }
						fluid={ image.childImageSharp.fluid }
					/>
				</div>
			</div>
		);
	}

	renderVideo () {
		const { id, author } = this.shuffledTrailer[0];

		return (
			<React.Fragment>
				<div className={ Styles.vimeoWrapper }>
					<iframe
						src={ `https://player.vimeo.com/video/${ id }?autoplay=1&title=0&byline=0&portrait=0` }
					    className={ Styles.vimeoIframe }
						frameBorder="0"
					    allow="autoplay; fullscreen"
						allowFullScreen
					/>
				</div>

				<Heading subtitle className={ "has-margin-top-sm has-text-white" }>
					<span dangerouslySetInnerHTML={ { __html: author } } />
				</Heading>
			</React.Fragment>
		)
	}

	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
				        hero: markdownRemark (frontmatter: { section: { eq: "hero" } }) {
				            frontmatter {
				                overlay {
				                    title
				                    subtitle
				                }
				
				                trailer {
				                    id
				                    author
				                }
				
				                slides {
				                    title
				                    image {
				                        childImageSharp {
				                            fluid {
				                                ...GatsbyImageSharpFluid
				                            }
				                        }
				                    }
				                }
				            }
				        }
				    }
				` }
				render={ this.renderContent.bind(this) }
			/>
		)
	}
}