import React, { Component } from "react";
import PropTypes from "prop-types";

import Styles from "./index.module.scss";
import "./index.scss";


export default class Map extends Component {
	static propTypes = {
		room: PropTypes.string,
		building: PropTypes.string
	};

	static defaultProps = {};

	svgRef = React.createRef();
	lastBuilding = null;
	lastRoom = null;

	componentDidUpdate (prevProps, prevState, snapshot) {
		const { current } = this.svgRef;

		if (current) {
			if (this.lastBuilding) {
				this.lastBuilding.classList.remove("active");
			}

			this.lastBuilding = current.querySelector(`#building-${ this.props.building }`);

			if (this.lastBuilding) {
				this.lastBuilding.classList.add("active");
			}

			// -------------

			if (this.lastRoom) {
				this.lastRoom.classList.remove("active");
			}

			this.lastRoom = current.querySelector(`#${ this.props.building }-${ this.props.room }`);

			if (this.lastRoom) {
				this.lastRoom.classList.add("active");
			}
		}
	}

	componentDidMount () {

	}

	render () {
		return (
			<div className={ Styles.svgWrapper }>
				<svg ref={ this.svgRef } width="608px" height="833px" viewBox="0 0 608 833" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
					<g id="Components" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
						<g id="comp-/-map" transform="translate(-211.000000, -148.000000)">
							<g id="export-/-misc-/-map-/-areas-/-d" transform="translate(210.768207, 148.426333)" fill="#52ACE4">
								<g id="comp/buildings/d" transform="translate(78.651942, 0.822653)">
									<polygon id="building-d" points="1.13686838e-13 0 91.0971982 0 91.0971982 49.8930372 91.0971982 155.617222 42.3528537 155.617222 42.3528537 51.1168044 1.13686838e-13 51.1168044"></polygon>
								</g>
								<g id="comp/buildings/lw" transform="translate(0.859010, 0.822653)">
									<path d="M58.9796113,436.437445 L85.6814975,436.437445 L85.6814975,527.990793 L79.011485,527.990793 L79.011485,556.968427 L35.3242579,556.968427 L35.3242579,527.990793 L1.0658141e-13,527.990793 L1.0658141e-13,0 L58.9796113,0 L58.9796113,436.437445 Z M58.9796113,344.059479 L85.6814975,344.059479 L85.6814975,260.669346 L58.9796113,260.669346 L58.9796113,344.059479 Z M58.9796113,155.642291 L85.6814975,155.642291 L85.6814975,71.406354 L58.9796113,71.406354 L58.9796113,155.642291 Z" id="building-lw"></path>
								</g>
								<g id="comp/buildings/a" transform="translate(78.351242, 189.436265)">
									<polygon id="building-a" points="4.26325641e-14 -1.42108547e-14 91.3978982 -1.42108547e-14 91.3978982 48.5049884 91.3978982 156.216114 42.6867827 156.216114 42.6867827 52.0186827 4.26325641e-14 52.0186827"></polygon>
								</g>
								<g id="comp/buildings/hg" transform="translate(226.356603, 0.284720)">
									<polygon id="building-hg" points="157.705707 0 157.705707 224.409609 38.3325717 224.409609 -5.68434189e-14 224.409609 -5.68434189e-14 106.467777 38.3325717 106.467777 38.3325717 0"></polygon>
								</g>
								<g id="comp/buildings/1" transform="translate(0.925202, 607.482506)">
									<polygon id="building-1" points="-8.8817842e-14 171.236248 56.9714669 171.236248 56.9714669 0 -8.8817842e-14 0"></polygon>
								</g>
								<g id="comp/buildings/2" transform="translate(112.868375, 578.789879)">
									<polygon id="building-2" points="1.20792265e-13 199.554801 56.9714669 199.554801 56.9714669 -5.68434189e-14 1.20792265e-13 -5.68434189e-14"></polygon>
								</g>
								<g id="comp/buildings/3" transform="translate(226.040445, 579.262359)">
									<polygon id="building-3" points="-2.84217094e-14 55.4520124 158.174325 55.4520124 158.174325 -5.68434189e-14 -2.84217094e-14 -5.68434189e-14"></polygon>
								</g>
								<g id="comp/buildings/4" transform="translate(441.270167, 585.602997)">
									<polygon id="building-4" points="-5.68434189e-14 193.456047 54.9717036 193.456047 54.9717036 -5.68434189e-14 -5.68434189e-14 -5.68434189e-14"></polygon>
								</g>
								<g id="comp/buildings/5" transform="translate(553.294282, 586.599175)">
									<polygon id="building-5" points="-5.68434189e-14 191.964846 54.9717036 191.964846 54.9717036 0 -5.68434189e-14 0"></polygon>
								</g>
								<g id="comp/buildings/17" transform="translate(552.942101, 300.575969)">
									<polygon id="building-17" points="-5.68434189e-14 200.088719 41.5641068 200.088719 41.5641068 0 -5.68434189e-14 0"></polygon>
								</g>
								<g id="comp/buildings/s" transform="translate(429.425290, 217.089276)">
									<polygon id="building-s" points="1.42108547e-13 103.642992 19.8258603 103.642992 19.8258603 0 1.42108547e-13 0"></polygon>
								</g>
								<g id="comp/buildings/ibz" transform="translate(252.485544, 711.174761)">
									<polygon id="building-ibz" points="5.67238014 0.00720925556 5.67238014 8.2072225 7.10542736e-14 8.2072225 7.10542736e-14 91.95045 5.69548714 91.95045 5.69548714 120.766617 39.3634311 120.766617 39.3634311 107.714517 65.7339753 115.375381 101.832828 115.375381 101.832828 88.6282705 96.2793581 88.6282705 96.2793581 68.3282943 59.5937433 68.3282943 59.5937433 76.881561 38.7091653 76.881561 38.7091653 -5.68434189e-14"></polygon>
								</g>
							</g>
							<g id="rooms" transform="translate(288.774100, 149.188817)" fill="#FF7495">
								<g id="d-011" transform="translate(0.654209, 0.075481)">
									<rect id="Rectangle-Copy" x="0" y="0" width="64" height="40"></rect>
								</g>
								<g id="d-082" transform="translate(1.000000, 40.000000)">
									<rect id="Rectangle-Copy" x="0" y="0" width="64" height="12"></rect>
								</g>
								<g id="d-080" transform="translate(65.000000, 0.000000)">
									<rect id="Rectangle-Copy" x="0" y="0" width="27" height="52"></rect>
								</g>
								<g id="d-005" transform="translate(43.000000, 65.000000)">
									<rect id="Rectangle-Copy" x="0" y="0" width="22" height="53"></rect>
								</g>
								<g id="d-002" transform="translate(73.000000, 65.000000)">
									<rect id="Rectangle-Copy" x="0" y="0" width="19" height="51"></rect>
								</g>
								<g id="d-003" transform="translate(43.000000, 126.000000)">
									<rect id="Rectangle-Copy" x="0" y="0" width="49" height="30"></rect>
								</g>
							</g>
						</g>
					</g>
				</svg>
			</div>
		);
	}
}
