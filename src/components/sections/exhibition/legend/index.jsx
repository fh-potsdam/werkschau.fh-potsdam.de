import React, { Component } from "react";
import PropTypes from "prop-types";

import Heading from "react-bulma-components/lib/components/heading/heading";
import Columns from "react-bulma-components/lib/components/columns/columns";

import Styles from "./index.module.scss";


export default class Legend extends Component {
	static propTypes = {
		studyNames: PropTypes.any.isRequired,
		buildings: PropTypes.any.isRequired,
		projectTypes: PropTypes.any.isRequired,
		courses: PropTypes.any.isRequired,

		onProjectChange: PropTypes.func.isRequired,
		onBuildingChange: PropTypes.func.isRequired
	};

	state = {
		activeBuilding: null,
		activeRoom: null
	};

	handleProjectMouseOver (activeRoom) {
		this.setState({
			...this.state,
			activeRoom: activeRoom
		});

		this.props.onProjectChange(activeRoom);
	}

	handleProjectMouseOut () {
		this.setState({
			...this.state,
			activeRoom: null
		});
	}

	handleBuildingMouseOver (activeBuilding) {
		if (this.state.activeBuilding === activeBuilding) {
			return false;
		}

		this.setState({
			...this.state,
			activeBuilding
		});

		this.props.onBuildingChange(activeBuilding);
	}

	renderBuildings () {
		return (
			<Columns className={ Styles.legend }>
				{ this.props.courses.map(({ house, projects }) =>
					<React.Fragment key={ house }>
						<Columns.Column
							onMouseOver={ this.handleBuildingMouseOver.bind(this, house) }>
							<Columns
								multiline={ false }
								className={ this.generateBuildingClasses(house) }>
								<Columns.Column className={ Styles.buildingLabel }>
									<Heading
										className={ `is-uppercase ${ Styles.label }` }
										size={ 4 }
										renderAs={ "h2" }>
										{ house }
									</Heading>
								</Columns.Column>

								<Columns.Column>
									<Heading
										className={ Styles.labelFull }
										size={ 4 }
										renderAs={ "h2" }>
										{ this.props.buildings[house] }
									</Heading>

									{ this.renderProjects(projects) }
								</Columns.Column>
							</Columns>
						</Columns.Column>
					</React.Fragment>
				) }
			</Columns>
		);
	}
	
	render () {
		return this.renderBuildings();
	}

	renderProjects (projects) {
		return (
			<ul className={ Styles.projectsList }>
				{ projects.map(({ name, description, study, room, type }) =>
					<li
						key={ name }
						onMouseOver={ this.handleProjectMouseOver.bind(this, room) }
						onMouseOut={ this.handleProjectMouseOut.bind(this) }
						className={ this.generateProjectClasses(room) }>
						<p className={ `is-size-4 ${ Styles.name }` }>
							{ this.generateProjectLabel(type, name) }
						</p>

						{ room &&
							<p className={ `is-size-4 ${ Styles.room }` }>
								Raum { room }
							</p>
						}
					</li>
				) }
			</ul>
		);
	}

	generateBuildingClasses (name) {
		const classNames = [
			Styles.building
		];

		if (this.state.activeBuilding !== null && this.state.activeBuilding === name) {
			classNames.push(Styles.active);
		}

		return classNames.join(" ");
	}

	generateProjectClasses (room) {
		const classNames = [
			Styles.project
		];

		if (this.state.activeRoom !== null && this.state.activeRoom === room) {
			classNames.push(Styles.active);
		}

		return classNames.join(" ");
	}

	generateProjectLabel (type, name) {
		if (this.props.projectTypes[type]) {
			return `${ this.props.projectTypes[type] } » ${ name } «`;
		}

		return name;
	}
}
