import React, { Component } from "react";

import Container from "react-bulma-components/lib/components/container/container";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Columns from "react-bulma-components/lib/components/columns/columns";
import Content from "react-bulma-components/lib/components/content/content";

import Section from "../../layout/section";
import Legend from "./legend";
import Map from "./map";
import IconSrc from "./icon.svg";
import { graphql, StaticQuery } from "gatsby";


export default class Exhibition extends Component {
	state = {
		building: null,
		room: null
	};

	renderContent ({ exhibition }) {
		return (
			<Section
				id={ exhibition.frontmatter.section }
				isDark>
				<Container>
					{ this.renderHeader(exhibition.frontmatter.title, exhibition.html) }
					{ this.renderMap(exhibition.frontmatter) }
				</Container>
			</Section>
		);
	}

	handleProjectChange (room) {
		this.setState({
			...this.state,
			room
		});
	}

	handleBuildingChange (building) {
		this.setState({
			...this.state,
			building
		});
	}

	renderMap ({ studyNames, buildings, courses, projectTypes }) {
		return (
			<Columns className={ "has-margin-top-xl has-margin-bottom-xl" }>
				<Columns.Column narrow>
					<Legend
						studyNames={ studyNames }
						buildings={ buildings }
						courses={ courses }
						projectTypes={ projectTypes }
						onProjectChange={ this.handleProjectChange.bind(this) }
						onBuildingChange={ this.handleBuildingChange.bind(this) }
					/>
				</Columns.Column>

				<Columns.Column>
					<Map { ...this.state } />
				</Columns.Column>
			</Columns>
		);
	}

	renderHeader (title, content) {
		return (
			<React.Fragment>
				<Columns centered gapless>
					<Columns.Column narrow>
						<img
							src={ IconSrc }
							alt="Sektion: Die Ausstellung"
						/>
					</Columns.Column>
				</Columns>

				<Columns centered gapless>
					<Columns.Column>
						<Heading
							size={ 1 }
							className={ "has-text-centered has-text-white has-margin-bottom-none" }
							renderAs={ "h1" }>
							{ title }
						</Heading>
					</Columns.Column>
				</Columns>

				<Columns centered gapless>
					<Columns.Column className={ "is-half-desktop is-one-third-tablet" }>
						<Content
							size={ "medium" }
							className={ "has-text-centered has-text-grey" }
							dangerouslySetInnerHTML={ { __html: content } }
						/>
					</Columns.Column>
				</Columns>
			</React.Fragment>
		);
	}

	render () {
		return (
			<StaticQuery query={ graphql`
				query {
					exhibition: markdownRemark (frontmatter: { section: { eq: "exhibition" } }) {
						frontmatter {
							section
							title
			
			                buildings {
			                    d
			                    a
			                    hg
			                    lw
			                }
			
			                projectTypes {
			                    course
			                }
			
			                studyNames {
			                    id
			                    kd
			                    pd
			                    bd
				                md
			                }
							
			                courses {
			                    house
			                    projects {
			                        name
			                        description
			                        study
			                        room
			                        type
			                    }
			                }
						}
						
						html
					}
				}` }
				render={ this.renderContent.bind(this) }
			/>
		);
	}
}
