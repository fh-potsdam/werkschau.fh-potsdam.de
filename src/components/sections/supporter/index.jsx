import React, { Component } from "react";
import { StaticQuery, graphql } from "gatsby";

import Container from "react-bulma-components/lib/components/container/container";
import Content from "react-bulma-components/lib/components/content/content";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Columns from "react-bulma-components/lib/components/columns/columns";
import ButtonGroup from "react-bulma-components/lib/components/button/components/button-group";
import Button from "react-bulma-components/lib/components/button/button";

import Section from "../../layout/section";
import Styles from "./index.module.scss";

const Supporter = () =>
	<StaticQuery query={ graphql`
		query {
			supporter: markdownRemark (frontmatter: { section: { eq: "supporter" } }) {
				frontmatter {
	                section
	                title
	                description
	
	                supporterCTA {
		                description
		                label
		                link 
	                }
	
	                supporter {
	                    name
		                fullSize
	                    image
	                }
				}
			} 
		}` }
        render={ ({ supporter: sp }) => {
	        const {
		        section,
		        title,
		        description,
		        supporter,
		        supporterCTA
	        } = sp.frontmatter;

	        const {
		        description: ctaDesc,
		        label: ctaLabel,
		        link: ctaLink
	        } = supporterCTA;

	        return (
		        <Section id={ section }>
			        <Container>
				        <Heading
					        size={ 1 }
					        className={ "has-text-centered has-text-weight-bold" }
					        renderAs={ "h1" }>
					        { title }
				        </Heading>

				        <Columns centered>
					        <Columns.Column className={ "is-three-fifths-tablet is-one-third-desktop" }>
						        <Content
							        className={ `has-text-centered ${ Styles.description }` }
							        size={ "medium" }
							        dangerouslySetInnerHTML={ { __html: description } }
						        />
					        </Columns.Column>
				        </Columns>

				        <Columns centered multiline>
					        <Columns.Column className={ `is-one-half-tablet is-8-desktop has-text-centered ${ Styles.supporterWrapper }` }>
						        { supporter.map(({ name, image, fullSize }) =>
							        <img
								        key={ name }
								        src={ image }
								        alt={ name }
								        data-is-full-size={ fullSize || null }
								        className={ Styles.supporterImg }
							        />
						        ) }
					        </Columns.Column>
				        </Columns>

				        <Columns centered>
					        <Columns.Column className={ "has-margin-top-xl is-three-fifths-tablet is-two-fifths-desktop" }>
						        <Content
							        className={ `has-text-centered ${ Styles.description } is-italic` }
							        size={ "medium" }
							        dangerouslySetInnerHTML={ { __html: ctaDesc } }
						        />

						        <ButtonGroup position={ "centered" }>
							        <Button
								        renderAs={ "a" }
								        href={ ctaLink }
								        target={ "_blank" }
								        size={ "medium" }
								        color={ "info" }>
								        { ctaLabel }
							        </Button>
						        </ButtonGroup>
					        </Columns.Column>
				        </Columns>
			        </Container>
		        </Section>
	        );
        } }
	/>;

export default Supporter;