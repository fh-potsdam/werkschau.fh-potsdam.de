import React, { Component } from "react";
import { StaticQuery, graphql } from "gatsby";

import Container from "react-bulma-components/lib/components/container/container";
import Columns from "react-bulma-components/lib/components/columns/columns";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Content from "react-bulma-components/lib/components/content/content";
import ButtonGroup from "react-bulma-components/lib/components/button/components/button-group";
import Button from "react-bulma-components/lib/components/button/button";

import Section from "../../layout/section";
import Calendar from "./calendar";

import Styles from "./index.module.scss";


export default class Program extends Component {
	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
						program: markdownRemark (frontmatter: { section: { eq: "program" } }) {
							frontmatter {
				                section
				                title
				                description
				                
				                pdfButton {
									label
									link
								}
								
								dates {
				                    name
				                    subtitle
				                    events {
				                        title
				                        
						                description {
					                        content: childMarkdownRemark {
						                        html
					                        }
				                        }
						                
				                        room
				                        roomDisplayTitle
				                        start
				                        end
						                hideEndLabel
					                }
								}
							}
						} 						
					}
                ` }
				render={ Program.renderContent }
			/>
		);
	}

	static renderContent ({ program }) {
		const {
			title,
			section,
			description,
			pdfButton
		} = program.frontmatter;

		return (
			<Section id={ section }>
				<Container>
					<Columns
						centered
						className={ Styles.sectionHeader }>

						<Columns.Column
							narrow
							className={ "is-three-fifths-tablet is-two-fifths-desktop has-margin-bottom-lg" }>

							<Heading
								className={ "has-margin-bottom-lg has-text-weight-bold has-text-centered" }
								size={ 1 }
								renderAs={ "h1" }>
								{ title }
							</Heading>

							<Content
								size={ "medium" }
								className={ "has-text-centered" }
								dangerouslySetInnerHTML={ { __html: description } }
							/>

							<ButtonGroup position={ "centered" }>
								<Button
									color={ "link" }
									renderAs={ "a" }
									size={ "medium" }
									href={ pdfButton.link }
									target={ "_blank" }>
									{ pdfButton.label }
								</Button>
							</ButtonGroup>
						</Columns.Column>
					</Columns>

					<Calendar />
				</Container>
			</Section>
		);
	}
}