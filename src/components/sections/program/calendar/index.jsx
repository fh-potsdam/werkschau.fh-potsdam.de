import React, { Component } from "react";
import { graphql, StaticQuery } from "gatsby";
import Moment from "moment-timezone";

import Columns from "react-bulma-components/lib/components/columns/columns";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Content from "react-bulma-components/lib/components/content/content";

import Styles from "./index.module.scss";
import { applyHangingPunctuation } from "../../../helper";


export default class Calendar extends Component {
	state = {};

	componentDidMount () {
		// triggering rerender every minute for displaying current active event
		if (typeof window !== "undefined") {
			window.setInterval(() => {
				this.setState(this.state);
			}, 60000);
		}
	}

	render () {
		return (
			<StaticQuery query={
				graphql`
				    query {
				        exhibition: markdownRemark (frontmatter: { section: { eq: "exhibition" } }) {
				            frontmatter {
				                buildings {
				                    d
				                    a
				                    hg
				                    lw
				                }
				            }
				        }
				        
				        program: markdownRemark (frontmatter: { section: { eq: "program" } }) {
							frontmatter {
				                dates {
				                    name
				                    subtitle
				                    turtleTitle
				                    events {
				                        title
				                        description {
					                        content: childMarkdownRemark {
					                        	frontmatter {
					                        		timeSlots {
					                        			timeLabel
					                        			content
					                        		}
					                        	}
						                        html
					                        }
				                        }
				                        room
				                        roomDisplayTitle
				                        start
				                        end
				                        hideEndLabel
					                }
								}
							}
						}
				    }
				` }
	            render={ props =>
		            <Columns>
			            { this.renderDays(props) }
		            </Columns>
	            }
			/>
		);
	}

	renderDays ({ exhibition, program }) {
		return program.frontmatter.dates.map(({ name, subtitle, turtleTitle, events }, index) =>
			<Columns.Column
				key={ `${ index }-${ name }` }
				className={ "is-half-desktop" }>
				<Heading
					size={ 3 }
					className={ "has-text-centered has-text-weight-bold" }
					renderAs={ "h2" }>
					{ name }
				</Heading>

				<Heading
					size={ 4 }
					renderAs={ "h4" }
					className={ "has-text-centered has-text-grey has-text-weight-semibold has-margin-bottom-lg" }
					subtitle>
					{ subtitle }
				</Heading>

				<Heading
					size={ 4 }
					renderAs={ "h4" }
					className={ "has-text-centered has-text-grey has-text-weight-semibold has-margin-bottom-lg" }
					subtitle>
					{ turtleTitle }
				</Heading>

				{ this.renderDayEvents(events, exhibition) }
			</Columns.Column>
		);
	}

	renderDayEvents (events, exhibition) {
		return events.map(({ title, description, room, roomDisplayTitle, start, end, hideEndLabel = false }, index) => {
			const
				s = Moment(start),
				e = Moment(end),
				eventStarted = Moment(new Date()).isBetween(s, e);

			const eventActiveClass = eventStarted ? " " + Styles.eventActive : "";

			return (
				<div
					key={ `${ index }-${ title }` }
					className={ `${ Styles.event }${ eventActiveClass }` }>
					<div className={ Styles.timeData }>
						<Heading
							size={ 4 }
							className={ "has-margin-bottom-xs has-text-weight-bold" }>
							{ Moment(start).format("HH:mm") }
						</Heading>

						{ !hideEndLabel &&
							<Heading
								className={ `has-text-weight-bold ${ Styles.subtitle }` }
								size={ 6 }>
								{ e.format("[bis] HH:mm [Uhr]") }
							</Heading>
						}
					</div>

					<div className={ Styles.eventData }>
						<Heading
							size={ 4 }
							className={ `has-margin-bottom-xs has-text-weight-bold ${ Styles.title }` }
							dangerouslySetInnerHTML={ { __html: applyHangingPunctuation(title) } }
						/>

						{ (room || roomDisplayTitle ) &&
							<Heading
								className={ `has-text-weight-bold ${ Styles.subtitle }` }
								size={ 6 }>
								{ roomDisplayTitle ?
									roomDisplayTitle:
									<span>{ exhibition.frontmatter.buildings[room.split("-")[0]] } ({ room.toUpperCase() })</span>
								}
							</Heading>
						}

						{ description &&
							<Content
								className={ "has-margin-top-md" }
								dangerouslySetInnerHTML={ { __html: applyHangingPunctuation(description.content.html) } }
							/>
						}

						{ description && description.content.frontmatter.timeSlots &&
							Calendar.renderTimeSlots(description.content.frontmatter.timeSlots)
						}
					</div>
				</div>
			);
		});
	}

	static fixHangingPunctuation (string) {
		return applyHangingPunctuation(string);
	}

	static renderTimeSlots (timeSlots) {
		return timeSlots.map(({ timeLabel, content }, index) =>
			<div
				key={ `${ index }-${ timeLabel }` }
				className={ Styles.timeSlotWrapper }>
				<Heading
					className={ `has-text-weight-bold ${ Styles.timeLabel } ${ Styles.subtitle }` }
					size={ 6 }>
					{ timeLabel }
				</Heading>

				<Content dangerouslySetInnerHTML={ { __html: content } } />
			</div>
		);
	}
}
