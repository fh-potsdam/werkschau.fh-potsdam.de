import React, { Component } from "react";

import Container from "react-bulma-components/lib/components/container/container";
import Columns from "react-bulma-components/lib/components/columns/columns";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Content from "react-bulma-components/lib/components/content/content";
import { GoogleMap, Marker, OverlayView, withGoogleMap, withScriptjs } from "react-google-maps";

import Styles from "./index.module.scss";
import Section from "../../layout/section";
import MapStyles from "./map-styles.json";
import { graphql, StaticQuery } from "gatsby";


const Position = {
	lat: 52.413362,
	lng: 13.051114
};

const MapUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAms6iDjU5U8a5_Hm6WSBN-DbvetTCAqBs&3.exp";

const getPixelPositionOffset = (width, height) => ({
	x: -(width / 2),
	y: -(height / 2)
});

const Map = withScriptjs(withGoogleMap(props =>
	<GoogleMap
		defaultZoom={ 17 }
		defaultCenter={ Position }
		defaultOptions={ {
			styles: MapStyles,
			disableDefaultUI: true
		} }>

		<Marker position={ Position } />
		<OverlayView
			position={ Position }
			mapPaneName={ OverlayView.OVERLAY_MOUSE_TARGET }
			getPixelPositionOffset={ getPixelPositionOffset }>
			<div className={ Styles.mapOverlay }>
				<Heading>
					{ props.overlay.headline }
				</Heading>

				<Content
					size={ "medium" }
					className={ Styles.overlayContent }>
					<p>{ props.overlay.street }</p>
					<p>{ props.overlay.postalCode } { props.overlay.city }</p>
				</Content>
			</div>
		</OverlayView>
	</GoogleMap>
));

export default class Journey extends Component {
	renderContent ({ journey }) {
		const {
			title,
			section,
			description,
			travelTypes,
			overlay
		} = journey.frontmatter;

		return (
			<Section
				id={ section }
				isDark
				hasBottomSeparator={ false }>
				<Container>
					<Columns multiline>
						<Columns.Column className={ "is-full" }>
							<Heading
								size={ 1 }
								className={ "has-text-white has-text-weight-bold has-margin-bottom-sm" }
								renderAs={ "h1" }>
								{ title }
							</Heading>
						</Columns.Column>

						<Columns.Column size={ "half" }>
							<Content
								size={ "medium" }
								className={ "has-text-grey has-margin-bottom-lg" }
								dangerouslySetInnerHTML={ { __html: description } }
							/>
						</Columns.Column>
					</Columns>

					{ travelTypes &&
						<Columns className={ "has-margin-bottom-lg" }>
							{ travelTypes.map(({ headline, description, link, linkLabel }) =>
								<Columns.Column key={ headline } size={ "half" }>
									<Heading className={ "has-margin-bottom-md has-text-weight-bold" } size={ 5 } renderAs={ "h2" }>
										<span className={ "has-background-grey has-padding-sm" }>
											{ headline }
										</span>
									</Heading>

									<Content size={ "medium" } className={ "has-text-grey" }>
										<p>{ description }</p>

										{ link && linkLabel &&
											<p>
												<a href={ link } target={ "_blank" }>→ { linkLabel }</a>
											</p>
										}
									</Content>
								</Columns.Column>
							) }
						</Columns>
					}

					<Map
						googleMapURL={ MapUrl }
						loadingElement={ <div style={{ height: `100%` }} /> }
						containerElement={ <div style={{ height: `70vh` }} /> }
						mapElement={ <div style={{ height: `100%` }} /> }
						overlay={ overlay }
					/>
				</Container>
			</Section>
		);
	}

	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
						journey: markdownRemark (frontmatter: { section: { eq: "journey" } }) {
							frontmatter {
								section
								title
				                description
				
								travelTypes { 
									headline
									description
									link
									linkLabel
								}
								
								overlay {
									headline
									street
									postalCode
									city
								}
							}
						}
					}	
				` }
				render={ this.renderContent.bind(this) }
			/>
		)
	}
}