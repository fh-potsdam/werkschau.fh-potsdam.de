import React, { Component } from "react";
import Slider from "react-slick";
import { StaticQuery, graphql } from "gatsby";

import Container from "react-bulma-components/lib/components/container/container";
import Columns from "react-bulma-components/lib/components/columns/columns";
import Heading from "react-bulma-components/lib/components/heading/heading";
import Content from "react-bulma-components/lib/components/content/content";
import Image from "react-bulma-components/lib/components/image/image";
import ButtonGroup from "react-bulma-components/lib/components/button/components/button-group";
import Button from "react-bulma-components/lib/components/button/button";

import Section from "../../layout/section";
import Styles from "./index.module.scss";
import { shuffleArray } from "../../helper";


export default class Werkschau extends Component {
	static propTypes = {};

	static defaultProps = {};

	static SLIDER_SETTINGS = {
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 500,
		dots: false,
		arrows: false,
		infinite: true,
		lazyLoad: "ondemand",
		pauseOnHover: false
	};

	state = {
		randomized: false
	};

	shouldComponentUpdate (nextProps, nextState, nextContext) {
		return !nextState.randomized;
	}

	componentDidUpdate (prevProps, prevState, snapshot) {
		if (!this.state.randomized) {
			this.setState({
				...this.state,
				randomized: true
			});
		}
	}

	renderContent ({ werkschau }) {
		const
			{ frontmatter } = werkschau,
			projects = shuffleArray(frontmatter.projects).slice(0, 8),
			halfWayThough = Math.floor(projects.length / 2),
			firstArray = projects.slice(0, halfWayThough),
			secondArray = projects.slice(halfWayThough, projects.length);

		return (
			<Section id={ frontmatter.section }>
				<Container>
					<Columns className={ `is-marginless` }>
						<Columns.Column size={ "half" } className={ Styles.description }>
							<Heading
								size={ 1 }
								className={ "has-margin-bottom-none has-text-weight-bold" }
								style={ { marginLeft: "-0.2rem" } }
								renderAs={ "h2" }>
								{ frontmatter.title }
							</Heading>

							<Heading
								size={ 5 }
								className={ `has-text-weight-bold ${ Styles.subtitle }` }
								renderAs={ "h3" }>
								{ frontmatter.subtitle }
							</Heading>

							<Content
								size={ "medium" }
								className={ "has-margin-bottom-lg" }
								dangerouslySetInnerHTML={ { __html: werkschau.html } }
							/>

							<ButtonGroup
								position={ "centered" }
								className={ "has-margin-bottom-xl" }>
								<Button
									renderAs={ "a" }
									href={ "/ics/werkschau-event.ics" }
									color={ "link" }
									size={ "medium" }>
									Zum Kalender hinzufügen
								</Button>
							</ButtonGroup>

							{ this.renderImages(frontmatter, firstArray, "left") }
						</Columns.Column>

						<Columns.Column size={ "half" }>
							{ this.renderImages(frontmatter, secondArray, "right") }
						</Columns.Column>
					</Columns>
				</Container>
			</Section>
		);
	}

	renderImages (frontmatterData, projects, offsetSide = "left") {
		const className = offsetSide === "left" ? Styles.imageWrapperLeft : Styles.imageWrapperRight;

		return projects.map((project, index) =>
			<div
				key={ index }
				className={ `${ className }` }>
				<Slider { ...Werkschau.SLIDER_SETTINGS }>
					{ project.images.map((src, index) =>
						<Image
							key={ index }
							src={ frontmatterData.projectsImgBaseDir + src }
							className={ `has-margin-bottom-xs ${ Styles.projectImage }` }
						/>
					) }
				</Slider>

				<Heading subtitle>
					{ project.name ?
						<React.Fragment>
							<b>»&thinsp;{ project.name }&thinsp;«</b> von { project.author }
						</React.Fragment>:

						<React.Fragment>
							Projekt von { project.author }
						</React.Fragment>
					}
				</Heading>
			</div>
		);
	}

	render () {
		return (
			<StaticQuery
				query={ graphql`
					query {
						werkschau: markdownRemark (frontmatter: { section: { eq: "werkschau" } }) {
							frontmatter {
								section
								title
								subtitle
				
				                projectsImgBaseDir
				                projects {
				                    name
				                    author
				                    images
				                }
							}
							
							html
						} 					
					}
				` }
				render={ this.renderContent.bind(this) }
			/>
		);
	}
}
