import React from "react";
import { graphql } from "gatsby";
import { BreakpointProvider } from "react-socks";
import Content from "react-bulma-components/lib/components/content/content";
import Container from "react-bulma-components/lib/components/container/container";
import Columns from "react-bulma-components/lib/components/columns/columns";

import Page from "../components/layout/page";
import Footer from "../components/layout/footer";


export default ({ data }) => (
	<BreakpointProvider>
		<Page
			seo={ data.general.frontmatter.seo }
			nav={ data.general.frontmatter.navItems }>

			<Container className={ "has-margin-top-xxl has-margin-bottom-xxl" }>
				<Columns>
					<Columns.Column size={ "two-thirds" }>
						<Content
							size={ "medium" }
							dangerouslySetInnerHTML={{ __html: data.dataPrivacy.html }}
						/>
					</Columns.Column>
				</Columns>
			</Container>

			<Footer footerItems={ data.general.frontmatter.footerItems } />
		</Page>
	</BreakpointProvider>
);

export const query = graphql`
	query {
		general: markdownRemark (frontmatter: { section: { eq: "general" } }) {
			frontmatter {
				seo {
					title
					description
				}
				
				navItems {
					title
					link
				}
				
				footerItems {
					title
					items {
						title
						link
					}
				}
			}
		}

		dataPrivacy: markdownRemark (frontmatter: { section: { eq: "data-privacy" } }) {
			html
		} 
	}
`;