import React from "react";
import { graphql } from "gatsby";
import { BreakpointProvider } from "react-socks";

import Page from "../components/layout/page";
import Footer from "../components/layout/footer";
import Hero from "../components/sections/hero";
import Werkschau from "../components/sections/werkschau";
import Journey from "../components/sections/journey";
// import Exhibition from "../components/sections/exhibition";
import Supporter from "../components/sections/supporter";
import Program from "../components/sections/program";


export default ({ data }) => (
	<BreakpointProvider>
		<Page
			seo={ data.general.frontmatter.seo }
			nav={ data.general.frontmatter.navItems }>

			<Hero />
			<Werkschau />
			{/*<Exhibition { ...data.exhibition } />*/}
			<Program />
			<Supporter />
			<Journey />

			<Footer />
		</Page>
	</BreakpointProvider>
);

export const query = graphql`
	query {
        general: markdownRemark (frontmatter: { section: { eq: "general" } }) {
            frontmatter {
                seo {
                    title
                    description
                }

                navItems {
                    title
                    link
                }

                footerItems {
                    title
                    items {
                        title
                        link
                    }
                }
            }
        }
	}
`;