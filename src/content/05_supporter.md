---

section: "supporter"
title: "Förderer"
description: "Wir danken allen Förderern, die unsere Werkschau erst möglich gemacht haben!"

supporterCTA:
  description: "Sie wollen dabei sein, uns fördern und unsere Werkschau unterstützen? Sehr gern – Nehmen Sie Kontakt mit uns auf:"
  label: "Kontakt aufnehmen"
  link: "mailto:design@fh-potsdam.de?subject=Werkschau%20Förderung"

supporter:
 - name: "haus17"
   fullSize: true
   image: "/images/05_supporters/haus17.svg"
 - name: "mawa"
   image: "/images/05_supporters/mawa.svg"
 - name: "Anntrieb"
   image: "/images/05_supporters/anntrieb.svg"
 - name: "Studentenwerk"
   image: "/images/05_supporters/studentenwerk.png"
 - name: "VWK Future Center"
   image: "/images/05_supporters/vwk-future-center.svg"

---