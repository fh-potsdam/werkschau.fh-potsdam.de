---

section: "general"

seo:
 title: "Werkschau ’19" 
 description: "12.–13. Juli 2019 • Die Werkschau 2019 der FH Potsdam ist die studentisch geplante und realisierte Jahresausstellung des Fachbereichs Design. Hier haben Studierende aus den vier Studiengängen Interfacedesign, Kommunikationsdesign, Produktdesign und dem Master in Design die Möglichkeit, ihre Arbeiten und Projekte aus den vergangenen zwei Semestern auszustellen und zu präsentieren."

navItems: 
 - title: "Werkschau"
   link: "werkschau"
 - title: "Programm"
   link: "program"
 - title: "Förderer"
   link: "supporter"
 - title: "Anfahrt"
   link: "journey"

footerItems:
 - title: "Rechtliches"
   items: 
     - title: "Impressum"
       link: "/imprint"
     - title: "Datenschutz"
       link: "data-privacy"
 - title: "Weitere Links"
   items:
     - title: "Konterfei Festival"
       link: "http://konterfei-festival.de.www238.your-server.de/"
     - title: "Presse"
       link: "https://www.fh-potsdam.de/informieren/presse/"
 - title: "Social Media"
   items:
     - title: "Instagram"
       link: "https://www.instagram.com/werkschaufhp/"
     - title: "Facebook"
       link: "https://www.facebook.com/FHPotsdam.werkschau"
     - title: "Twitter"
       link: "https://twitter.com/FHPwerkschau" 

---