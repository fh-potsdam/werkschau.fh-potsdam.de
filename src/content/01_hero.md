---

section: "hero"

overlay:
  title: "12.–13. <br/>Juli ’19"
  subtitle: "Freitag, 15 bis 20 Uhr<br/>Samstag, 11 bis 20 Uhr"
  
trailer:
  - id: "343093866"
    author: "Trailer von Aline Sawalsky"
  - id: "343155650"
    author: "Trailer von Maryna Honcharenko"
  - id: "343194520"
    author: "Trailer von Aaron Schwerdtfeger, Moritz Riedl und Robin Wenzel"
  - id: "343186061"
    author: "Trailer von Moritz Gruhl"
  - id: "343176113"
    author: "Trailer von Oneka Meyer, Yü Jin und Maria Krafft"
  - id: "343432373"
    author: "Trailer von Kirill Tatarenkov"
  - id: "343224096"
    author: "Trailer von Marcus Schindler"
  - id: "343878951"
    author: "Trailer von Zinan Zhang"
  - id: "343103785"
    author: "Trailer von Muhammed Yapici"


slides:
 - title: "Grillen"
   image: "./hero/grillen.jpg"
 - title: "Plakate"
   image: "./hero/plakate.jpg"
 - title: "Bitte Weibchen Scheiße"
   image: "./hero/bitte_weibchen_scheisse.jpg"
 - title: "Pflanzen"
   image: "./hero/pflanzen.jpg"
 - title: "Ausstellungsaufbau"
   image: "./hero/foyer_ausstellung_aufbau.jpg"
 - title: "Kursvorbereitungen für die Werkschau"
   image: "./hero/typo_basis_2.jpg"
 - title: "Solarpavillon zur Werkschau"
   image: "./hero/solarpavillon.jpg"

---