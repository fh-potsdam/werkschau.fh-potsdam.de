---

section: "journey"

title: "Anfahrt"
description: "Die Parkmöglichkeiten auf unserem Campus sind begrenzt. Bitte nutzen Sie die Möglichkeit des öffentlichen Nahverkehrs."

travelTypes:
  - headline: "ÖPNV"
    description: "Ab Potsdam Hauptbahnhof (mit RE1, RB21 und S7 schnell aus Berlin zu erreichen) mit den Tramlinien 92 (Richtung Kirschallee) und 96 (Richtung Viereckremise) bis »Campus Fachhochschule«."
    link: "https://www.vbb.de/"
    linkLabel: "VBB Verbindungssuche" 

overlay: 
  headline: "FH Potsdam"
  street: "Kiepenheuerallee 5"
  postalCode: "14469"
  city: "Potsdam"
  
---