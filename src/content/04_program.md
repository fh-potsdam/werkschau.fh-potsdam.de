---

section: "program"
title: "Programm"
description: "Neben der Ausstellung finden Sie auch <br/>interessante Vorträge und Präsentationen in unseren&nbsp;Hörsälen."

pdfButton:
 label: "Programm als PDF"
 link: "/pdf/werkschau_programm.pdf"

dates:
 - name: "Freitag"
   subtitle: "12. Juli"
   turtleTitle: "Ausstellung von 15 bis 20 Uhr"
   events:

    - title: "Auftakt&shy;ver&shy;anstaltung 🥂"
      room: "d-011"
      roomDisplayTitle: "Hörsaal Haus D/011"
      start: "2019-07-12 15:00:00"
      end: "2019-07-12 16:00:00"
      hideEndLabel: true

    - title: "Start Konterfei-Festival 🎉"
      description: "./program/konterfei.md"
      roomDisplayTitle: "Campus Gelände"
      start: "2019-07-12 16:00:00"
      end: "2019-07-13 00:00:00"
      hideEndLabel: true

    - title: "POP UP WORKSHOP"
      description: "./program/popup-workshop.md"
      roomDisplayTitle: "LW/213 (Grafiklabor)"
      start: "2019-07-12 16:00:00"
      end: "2019-07-12 18:30:00"

    - title: "Werk<i>schlau</i> 💡"
      description: "./program/werkschlau.md"
      room: "d-011"
      roomDisplayTitle: "Hörsaal Haus D/011"
      start: "2019-07-12 17:00:00"
      end: "2019-07-12 20:00:00"

 - name: "Samstag"
   subtitle: "13. Juli"
   turtleTitle: "Ausstellung von 11 bis 20 Uhr"
   events:

    - title: "Alumni Runde"
      roomDisplayTitle: "Hörsaal Haus D/011"
      description: "./program/alumni.md"
      start: "2019-07-13 12:00:00"
      end: "2019-07-13 13:00:00"

    - title: "»Prototyping Futures«"
      roomDisplayTitle: "Hörsaal Haus D/011"
      description: "./program/prototyping-futures.md"
      start: "2019-07-13 13:30:00"
      end: "2019-07-13 19:30:00"

---