---

timeSlots:
 - timeLabel: "13:30 Uhr"
   content: "<b>PROTOTYP – Wie öffnen und schließen Prototypen mögliche&nbsp;Zukünfte?</b><br/>Jordi Tost, Paula Schuster (Interaction Design Lab IDL, FH Potsdam), Marcel Woznica (Soziologie, JGU Mainz), Dr. Andrea Schikowitz (Friedrich Schiedel-Stiftungslehrstuhl für Wissenschaftssoziologie, TU München)"
 - timeLabel: "15:00 Uhr"
   content: "<b>Prozesshaftigkeit von Gestaltung – Offenheit, Improvisation und&nbsp;Produktion</b><br/>Prof. Dr. Annika Frye (Designwissenschaft- und forschung, Muthesius Kunsthochschule Kiel)"
 - timeLabel: "16:00 Uhr"
   content: "<b>Die Praxis des Designs – Zur Soziologie arrangierter Ungewissheiten</b><br/>Dr. Valentin Janda (Techniksoziologie)"
 - timeLabel: "17:00 Uhr"
   content: "<b>Probes, Prompts, Participation – Neue Aufgaben von Prototyping in Forschungs- und Strategieprozessen</b><br/>Fabian Bitter (Designbasierte Strategieentwicklung, Fraunhofer CeRRI)"
 - timeLabel: "18:00 Uhr"
   content: "<b>Portents aus der Science Fiction – Spuren aus der Zukunft</b><br/>Dr. Bernd Flessner (Zukunftsforschung, Zentralinstitut für Wissenschaftsreflexion und Schlüsselqualifikationen ZiWiS, FAU Erlangen-Nürnberg)"
 - timeLabel: "19:00 Uhr"
   content: "<b>Kann man Zukunft ausstellen?</b><br/>Danny Könnicke (Ausstellungen / Sammlungen, Deutsches Museum, Zweigstelle Nürnberg)"

---

» Perspektiven auf Prototyping: Wie wir Zukünfte entwerfen «   
Weitere Infos zum Programm finden Sie auf unserer <a href='https://www.fh-potsdam.de/studieren/fachbereiche/design/oeffentlichkeit/news/event-detail/events/vortragsreihe-prototyping-futures/1727/' target='_blank'>FHP&nbsp;Website</a>