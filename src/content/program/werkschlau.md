Studierende, Alumni und Gründer*innen der FH Potsdam im Gespräch mit regionalen Firmen und Live Graphic Recording.
Moderation von Tobias Jänecke, Designer und Absolvent der FH&nbsp;Potsdam. 

Weitere Infos finden Sie auf der <a href='https://www.fh-potsdam.de/studieren/fachbereiche/design/oeffentlichkeit/veranstaltungen/event-detail/events/werkschlau-innovative-ideen-im-gespraech-1/1726/' target='_blank'>FHP&nbsp;Website</a>.