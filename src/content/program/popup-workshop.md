» ReLight: Lampen aus gefundenen Materialien «  

Im Rahmen des Kurses » Towns R Us « haben wir unser Projekt ReLight entwickelt, um leere Orte wiederzubeleben. Dafür bauen wir aus gefunden Materialien Lampenschirme, die Teil einer gemeinsamen Installation werden. 

**Ergänze unsere Lichterkette durch deinen eigenen Entwurf 😊**