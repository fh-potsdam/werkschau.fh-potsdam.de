---

section: "exhibition"

title: "Die Ausstellung"

studyNames:
  id: "Interfacedesign"
  kd: "Kommunikationsdesign"
  pd: "Produktdesign"
  bd:  "Bachelor Arbeit (Design)"
  md:  "Master Arbeit (Design)"
  
projectTypes:
  course: "Kurs"
  
buildings:
  d:  "Haus D"
  a:  "Haus A"
  hg: "Hauptgebäude"
  lw: "Labor und Werkstätten"

courses:
 - house: "d"
   projects: 
    - name: "White Cube"
      description: ""
      study: "id"
      room: "003"
      type: "course"
    - name: "Alles Großes Durcheinander"
      description: ""
      study: "kd"
      room: "002"
      type: "course"
 - house: "lw"
   projects: 
     - name: "MotionBasic"
       description: ""
       study: "pd"
       room: "005"
       type: "course"
     - name: "MotionStandard"
       description: ""
       study: "md"
       room: "011"
       type: "course"
	  
---

Vom 12. bis 13. Juli stehen unsere Türen von 10–18 Uhr für Sie offen.   
Detailierte Infos findet ihr in unserem <a href="/pdf/fhp-programmheft.pdf" target="_blank">Programmheft</a>.