---

section: "imprint"

---

<h1 class="entry-title">Impressum</h1>
<p><strong>Werkschau der Fachhochschule Potsdam</strong><br>
Fachbereich Design<br/>
Kiepenheuerallee 5<br>
14469 Potsdam<br>
<a href="mailto:werkschau@fh-potsdam.de">werkschau@fh-potsdam.de</a>
</p>
<p>&nbsp;</p>
<h2>DISCLAIMER – RECHTLICHE HINWEISE</h2>
<h3>§1 HAFTUNGSBESCHRÄNKUNG</h3>
<p>Eine Haftung für die Richtigkeit, Vollständigkeit und Aktualität dieser Webseiten kann trotz sorgfältiger Prüfung nicht übernommen werden. Die Werkschau übernimmt insbesondere keine Haftung für eventuelle Schäden oder Konsequenzen, die durch eine direkte oder indirekte Nutzung der angebotenen Inhalte entstehen. Es wird ausdrücklich darauf hingewiesen, dass für den Inhalt verlinkter Seiten ausschließlich deren Betreiber verantwortlich ist. Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für Schäden, die aus der Nutzung oder Nichtnutzung dieser Informationen entstehen, haftet ausschließlich der Betreiber der Seite, auf die verwiesen wurde. Namentlich gekennzeichnete Beiträge geben die Meinung des jeweiligen Autors und nicht immer die Meinung des Anbieters wieder. Mit der reinen Nutzung der Website des Anbieters kommt keinerlei Vertragsverhältnis zwischen dem Nutzer und dem Anbieter zustande.</p>
<p>Sollten Sie Kenntnis von verlinkten Seiten mit rechtswidrigen Inhalten erlangen, bitten wir Sie, uns dies mitzuteilen.</p>
<p>&nbsp;</p>
<h3>§2 EXTERNE LINKS</h3>
<p>Diese Website enthält Verknüpfungen zu Websites Dritter („externe Links“). Diese Websites unterliegen der Haftung der jeweiligen Betreiber. Der Anbieter hat bei der erstmaligen Verknüpfung der externen Links die fremden Inhalte daraufhin überprüft, ob etwaige Rechtsverstöße bestehen. Zu dem Zeitpunkt waren keine Rechtsverstöße ersichtlich. Der Anbieter hat keinerlei Einfluss auf die aktuelle und zukünftige Gestaltung und auf die Inhalte der verknüpften Seiten. Das Setzen von externen Links bedeutet nicht, dass sich der Anbieter die hinter dem Verweis oder Link liegenden Inhalte zu Eigen macht. Eine ständige Kontrolle der externen Links ist für den Anbieter ohne konkrete Hinweise auf Rechtsverstöße nicht zumutbar. Bei Kenntnis von Rechtsverstößen werden jedoch derartige externe Links unverzüglich gelöscht.</p>
<h3>§3 URHEBER- UND LEISTUNGSSCHUTZRECHTE</h3>
<p>Alle veröffentlichten Inhalte (Layout, Texte, Bilder, Grafiken usw.) wurden – soweit nicht anders ersichtlich von der Werkschau erstellt und unterliegen dem Urheberrecht. Die Inhalte dürfen damit zum Beispiel nur mit ausdrücklicher vorheriger Zustimmung der Werkschau kopiert, vervielfältigt, bearbeitet oder übersetzt werden. Auch die Einspeicherung, Verarbeitung bzw. Wiedergabe von Inhalten in Datenbanken oder anderen elektronischen Medien und Systemen ist nur mit Genehmigung des Rechteinhabers zulässig. Unberührt davon bleibt das „Kopieren“ der Dateien auf den eigenen Rechner, um sich die WWW-Seiten auf einem Browser anzuschauen. Ausgenommen sind ebenfalls die Pressemitteilungen. Sie sind dem Inhalt nach von jedermann frei und ohne besondere Genehmigung weiterverwendbar. Die Nutzung der anderen allgemein zugänglichen Texte – auch auszugsweise – durch Dritte ausschließlich zum privaten, wissenschaftlichen und nichtgewerblichen Gebrauch ist nur unter der Bedingung gestattet, dass die Werkschau als Urheber genannt wird. Die Nutzung der anderen Inhalte, wie beispielsweise der Bilder und Grafiken, ist nicht gestattet. Sollten Sie jedoch Interesse an einem Nutzungsrecht haben, wenden Sie sich bitte an <a href="mailto:nele.priebs@fh-potsdam.de">Nele Priebs</a>.</p>
<h3>§4 BESONDERE NUTZUNGSBEDINGUNGEN</h3>
<p>Soweit besondere Bedingungen für einzelne Nutzungen dieser Website von den vorgenannten Paragraphen abweichen, wird an entsprechender Stelle ausdrücklich darauf hingewiesen. In diesem Falle gelten im jeweiligen Einzelfall die besonderen Nutzungsbedingungen.</p>
<p>Quelle: Impressum-Generator von Impressum-recht.de</p>
<h2>BILDNACHWEISE</h2>
<p>Verwendung von Fotos mit freundlicher Genehmigung von Hüsna Gecer, Nele Priebs, Luisa Lessing, Helene Sellnau sowie Carolin Sprenger. Die Bildrechte liegen bei den Fotografen, unerlaubtes Verwenden von Bildmaterial ist untersagt.</p>
<h2>DATENSCHUTZERKLÄRUNG</h2>
<p>Nachfolgend möchten wir Sie über unsere Datenschutzerklärung informieren. Sie finden hier Informationen über die Erhebung und Verwendung persönlicher Daten bei der Nutzung unserer Webseite. Wir beachten dabei das für Deutschland geltende Datenschutzrecht. Sie können diese Erklärung jederzeit auf unserer Webseite abrufen.</p>
<p>Wir weisen ausdrücklich darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen und nicht lückenlos vor dem Zugriff durch Dritte geschützt werden kann.</p>
<p>Die Verwendung der Kontaktdaten unseres Impressums zur gewerblichen Werbung ist ausdrücklich nicht erwünscht, es sei denn wir hatten zuvor unsere schriftliche Einwilligung erteilt oder es besteht bereits eine Geschäftsbeziehung. Der Anbieter und alle auf dieser Website genannten Personen widersprechen hiermit jeder kommerziellen Verwendung und Weitergabe ihrer Daten.</p>
<p>Siehe <a href="https://www.fh-potsdam.de/datenschutzhinweis/">Datenschutzbestimmungen der FH Potsdam</a></p>
<p>&nbsp;</p>
<p><a href="/">Zurück zur Startseite</a></p>
	</div>

