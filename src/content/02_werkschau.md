---

section: "werkschau"
title: "Werkschau"
subtitle: "Jahresausstellung der FH Potsdam"

projectsImgBaseDir: "/images/02_werkschau/"
projects:
 - name: "Farbe und Form"
   description: ""
   author: "Milica Milojkovic"
   images: [
     "01 Milica Milojkovic/FHP_Jonathan Carl u. Milica Milojkovic.jpeg",
     "01 Milica Milojkovic/milica_gelbblau.jpeg",
     "01 Milica Milojkovic/milica_lilaorange-01 Kopie.jpeg",
   ]
 - name: "Impressionen"
   author: "Anna Zaretskaya"
   images: [
     "02 Anna Zaretskaya/IMG_3867_6.jpg",
     "02 Anna Zaretskaya/IMG_3863_2.jpg",
     "02 Anna Zaretskaya/IMG_3862_5.jpg",
     "02 Anna Zaretskaya/IMG_3860_3.jpg"
   ]
 - name: "rein_fühlen"
   author: "Rebekka Eversmann"
   images: [
     "04 Rebekka Eversmann/DSC_3354.jpeg",
     "04 Rebekka Eversmann/DSC_3464.jpeg"
   ]
 - name: "Socks"
   author: "Lisa Grüb"
   images: [
     "03 Lisa Grüb/Foto-06.jpg",
     "03 Lisa Grüb/Foto-05.jpg",
     "03 Lisa Grüb/Foto-04.jpg",
     "03 Lisa Grüb/Foto-03.jpg",
     "03 Lisa Grüb/Foto-01.jpg"
   ]
 - author: "Lukas Horn"
   images: [
     "07 Lukas Horn/Werkschau1_Lukas_Horn.jpg",
     "07 Lukas Horn/Werkschau2_Lukas_Horn.jpg",
     "07 Lukas Horn/Werkschau3_Lukas_Horn.jpg",
     "07 Lukas Horn/Werkschau4_Lukas_Horn.jpg",
     "07 Lukas Horn/Werkschau5_Lukas_Horn.jpg",
     "07 Lukas Horn/Werkschau6_Lukas_Horn.jpg"
   ]   
 - name: "Mission: Erde"
   author: "Moritz Gruhl, Patrick Schneider und Marius Claßen"
   images: [
     "08 Moritz Gruhl/team.png",
     "08 Moritz Gruhl/screen_7.png",
     "08 Moritz Gruhl/screen_3.png",
     "08 Moritz Gruhl/screen_2.png",
     "08 Moritz Gruhl/screen_1.png"
   ]    
 - name: "PUR"
   author: "Jana Mordhorst"
   images: [
     "09 Jana Mordhorst/PUR_farbproberot_190313_Vorschaubild.jpg",
     "09 Jana Mordhorst/IMG_0041.JPG",
     "09 Jana Mordhorst/IMG_0045.JPG",
     "09 Jana Mordhorst/IMG_0046.JPG"
   ]    
 - name: "Unsterblich."
   author: "Jan Schulz"
   images: [
     "10 Jan Schulz/PET-Fisch_01.jpg",
     "10 Jan Schulz/Fischskulptur.jpg",
     "10 Jan Schulz/1_Fischskelett.jpg",
     "10 Jan Schulz/1_Schildkröte.JPG",
     "10 Jan Schulz/Ausstellung_01.jpg",
     "10 Jan Schulz/Ausstellung_02.jpg"
   ]   
 - name: "Distanz wünscht Nähe"
   author: "Elisa Hammerbacher"
   images: [
     "11 Elisa Hammerbacher/DSC05579.jpeg",
     "11 Elisa Hammerbacher/DSC05613.jpeg",
     "11 Elisa Hammerbacher/DSC05617.jpeg",
     "11 Elisa Hammerbacher/DSC05628.jpeg"
   ] 
 - name: "Vaterland und Mutter Erde"
   author: "Maike Panz"
   images: [
     "12 Maike Panz/_DSC1412.jpg",
     "12 Maike Panz/_DSC1182.jpg",
     "12 Maike Panz/_DSC1382.jpg"
   ]

---

Die Werkschau ’19 der <a href="https://www.fh-potsdam.de" target="_blank">FH Potsdam</a> ist die studentisch geplante und realisierte Jahresausstellung des <a href="https://design.fh-potsdam.de/" target="_blank">Fachbereichs Design</a>. Hier haben Studierende aus den Studiengängen <a href="https://www.fh-potsdam.de/studieren/bewerbenstudienbewerbung/bachelorstudiengaenge/europaeische-medienwissenschaft-ba/" target="_blank">Euro&shy;päische Medien&shy;wissen&shy;schaft</a>, <a href="https://www.fh-potsdam.de/interfacedesign/" target="_blank">Inter&shy;facedesign</a>, <a href="https://www.fh-potsdam.de/studieren/fachbereiche/design/studiengaenge/kommunikationsdesign/" target="_blank">Kommu&shy;nikations&shy;design</a>, <a href="https://www.fh-potsdam.de/studieren/fachbereiche/design/studiengaenge/produktdesign/" target="_blank">Produkt&shy;design</a> und dem <a href="https://www.fh-potsdam.de/studieren/fachbereiche/design/studiengaenge/design-ma/" target="_blank">Master in Design</a> die Möglichkeit, ihre Arbeiten und Projekte aus den vergangenen zwei Semestern auszustellen und zu präsentieren. Freuen Sie sich auf über 60 verschiedene Kurse und einer Vielzahl entstandener Kursprojekte.

Wir freuen uns, am <i>12.&nbsp;Juli von 15 bis 20&nbsp;Uhr</i> und am <i>13.&nbsp;Juli von 11 bis 20&nbsp;Uhr</i> unsere Türen zu öffnen, für alle, die Interesse an Design und Innovation haben, sich inspirieren lassen wollen oder einfach nur neugierig&nbsp;sind.

Neben kreativem Input durch die Ausstellung findet zeitgleich das ebenfalls studentisch organisierte <a href="http://konterfei-festival.de.www238.your-server.de/" target="_blank">Campus Festival (Konterfei)</a> der FH Potsdam statt. Das Festival sorgt mit Musik, Essen, Trinken und Unterhaltungsprogramm für Stimmung und gute Laune.

<span class="has-text-weight-bold">Wir sind fleißig am Vorbereiten und freuen uns sehr, dass Sie und Ihr im Juli dabei&nbsp;seid!</span>